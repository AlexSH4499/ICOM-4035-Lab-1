package orderedStructures;

public class Arithmetic extends Progression {
	private double commonDifference; 
	
	public Arithmetic(double firstValue, double commonDifference) { 
		super(firstValue); 
		this.commonDifference = commonDifference; 
	}
	
	@Override
	public double nextValue() {
		if(!this.hasCalledFirstTerm())
			throw new IllegalStateException("First Term method has not been called");
        
		current = current + commonDifference; 
		return current;
	}

	@Override
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		result.append(this.getClass().getSimpleName());
		return result.toString() ;
	}
	

	public double getTerm(int n)
	{
		double term = this.firstValue() + (n-1) * this.commonDifference;
		return term;
	}
}
