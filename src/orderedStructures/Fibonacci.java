package orderedStructures;

public class Fibonacci extends Progression
{

	private double prev; 
	
	public Fibonacci() 
	{ 
		this(1); 
		prev = 0; 
	}
	
	private Fibonacci(double first)
	{
		super(first);
	}

	@Override
	public double nextValue()
	{
		if(!this.hasCalledFirstTerm())
			throw new IllegalStateException("First Term method has not been called");
        // add the necessary code here
		double temp = current;
		current += prev;
		prev = temp;
		
		return current;
	}
	
	@Override	
	public double firstValue()
	{ 
		double value = super.firstValue(); 
		//this.hasCalleFirstTerm = true;
		prev = 0; 
		return value; 
	}

}
