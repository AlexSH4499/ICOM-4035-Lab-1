package orderedStructures;

public class Geometric extends Progression {

	private double commonFactor; 
	
	public Geometric(double firstValue, double commonFactor) { 
		super(firstValue); 
		this.commonFactor = commonFactor; 
	}
	
	@Override
	public double nextValue() {
		if(!this.hasCalledFirstTerm())
			throw new IllegalStateException("First Term method has not been called");
        
		current = current * commonFactor; 
		return current;
	}

	@Override
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		
		result.append(this.getClass().getSimpleName());
		return result.toString() ;
	}
	
	public double getTerm(int n)
	{
		double term = this.firstValue() * Math.pow(this.commonFactor, n-1); 
		return term;
	}
}
